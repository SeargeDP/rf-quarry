package the_fireplace.rfquarry.mmplfiles;

import net.minecraft.world.IBlockAccess;

public interface IFramePipeConnection {
	boolean isPipeConnected(IBlockAccess blockAccess, int x1, int y1, int z1, int x2, int y2, int z2);
}
